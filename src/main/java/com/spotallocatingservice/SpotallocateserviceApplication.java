package com.spotallocatingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpotallocateserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotallocateserviceApplication.class, args);
	}

}
