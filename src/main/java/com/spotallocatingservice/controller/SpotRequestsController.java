package com.spotallocatingservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spotallocatingservice.dto.SpotRequestDTO;
import com.spotallocatingservice.service.SpotRequestsService;

@RestController
@RequestMapping("/spotAllocateService")
public class SpotRequestsController {

	@Autowired
	SpotRequestsService spotRequestsService;

	@PostMapping("/spotRequest")
	public ResponseEntity<String> spotRequest(@RequestBody SpotRequestDTO spotRequestDTO) {
		return new ResponseEntity<String>(spotRequestsService.spotRequest(spotRequestDTO), HttpStatus.OK);
	}

}
