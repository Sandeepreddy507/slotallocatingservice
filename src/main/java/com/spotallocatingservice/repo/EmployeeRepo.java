package com.spotallocatingservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spotallocatingservice.entity.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {
		

}
