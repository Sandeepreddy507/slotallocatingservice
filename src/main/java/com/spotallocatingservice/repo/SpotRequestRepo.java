package com.spotallocatingservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.spotallocatingservice.entity.SpotRequest;

@Repository
public interface SpotRequestRepo extends JpaRepository<SpotRequest, Integer> {
		

}
