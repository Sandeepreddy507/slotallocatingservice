package com.spotallocatingservice.exception;

import org.springframework.stereotype.Component;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@NoArgsConstructor
@Data
@Builder
public class GlobalException {

	private String message;
	private String statusCode;

	public GlobalException(String message, String statusCode) {
		this.message = message;
		this.statusCode = statusCode;
	}

}