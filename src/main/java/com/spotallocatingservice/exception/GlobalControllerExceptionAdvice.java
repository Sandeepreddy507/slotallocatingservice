package com.spotallocatingservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class GlobalControllerExceptionAdvice extends ResponseEntityExceptionHandler {

	private static final String message = "We are facing technical Issues, try after some time";
	private static final String statusCode = "500";

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<GlobalException> handleAllExceptions(Exception ex, WebRequest request) {
		return new ResponseEntity<>(GlobalException.builder().message(message).statusCode(statusCode).build(),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

}