package com.spotallocatingservice.service;

import org.springframework.stereotype.Service;

import com.spotallocatingservice.dto.SpotRequestDTO;

@Service
public interface SpotRequestsService {
	
	public String spotRequest(SpotRequestDTO spotRequestDTO);

}
