package com.spotallocatingservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spotallocatingservice.dto.SpotRequestDTO;
import com.spotallocatingservice.entity.Employee;
import com.spotallocatingservice.entity.SpotRequest;
import com.spotallocatingservice.repo.EmployeeRepo;
import com.spotallocatingservice.repo.SpotRequestRepo;

@Service
public class SpotRequestsServiceImpl implements SpotRequestsService {

	@Autowired
	private EmployeeRepo employeeRepo;

	@Autowired
	private SpotRequestRepo spotRequestRepo;

	public String spotRequest(SpotRequestDTO spotRequestDTO) {

		Employee e=employeeRepo.findById(spotRequestDTO.getEmpId()).get();
		spotRequestRepo.save(SpotRequest.builder().requestedForDate(spotRequestDTO.getRequestedForDate())
				.emmployee(e).build());
		
		return "Spot request submitted";
	}

}
